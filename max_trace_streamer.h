#pragma once
#ifndef _MAX_TRACE_STREAMER_H
#define _MAX_TRACE_STREAMER_H

#include "base_streamer.h"

class MaxTraceStreamer : public BaseStreamer {
	MICObjective m_obj;
	set<pair<double, size_t>> m_heap;
public:
	MaxTraceStreamer(size_t wsize, size_t k, MetricEvaluator* metric = nullptr)
		: BaseStreamer(wsize, k, metric), m_obj(m_cache) {
	}

	// Returns true if element was accepted into the cache,
	// false otherwise.
	bool add(const vector<double>& elt) {
		m_cache->add(m_time, elt);
		m_heap.insert(make_pair(m_cache->evaluate(m_time, m_time), m_time));
		m_time++;
		if (m_time > m_wsize) {
			size_t rid = m_time - m_wsize - 1;
			auto vec = m_cache->get(rid);
			m_heap.erase(make_pair(m_cache->evaluate(rid, rid), rid));
			m_cache->remove(rid);
		}
		return true;
	}

	double query() {
		m_obj.clear();
		for (auto it = m_heap.rbegin(); it != m_heap.rend(); it++) {
			m_obj.push(it->second);
			if (m_obj.length() == m_k) break;
		}
		return m_obj.value();
	}

	vector<size_t> solution() {
		// Call query first to populate the solution
		query();
		return m_obj.ids();
	}

	size_t size() {
		return min(m_cache->size(), m_k);
	}
};
#endif
