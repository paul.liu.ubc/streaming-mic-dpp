///*

#include <iostream>
#include <fstream>
#include <sstream>
#include <chrono>

#include <stdio.h>
#include <stdlib.h>

// Roll our own to be platform indep.
#include "getopt.h"

#include "data_source.h"
#include "greedy_streamer.h"
#include "threshold_streamer.h"
#include "max_trace_streamer.h"

using namespace std;

int main(int argc, char* argv[]) {
	extern char* optarg;
	static char usage[] = "usage: %s -f filename\n";

	int c, flags = 0;
	string filename;
	while ((c = getopt(argc, argv, "f:")) != -1) {
		switch (c) {
		case 'f':
			flags = 1;
			filename = string(optarg);
			break;
		case '?':
			break;
		}
	}

	if (flags < 1) {
		fprintf(stderr, "%s: missing input file\n", argv[0]);
		fprintf(stderr, usage, argv[0]);
		exit(1);
	}

	ifstream config_file(filename);
	size_t N, K, W, DIM, offset;
	string type, header;
	offset = 0;

	// Header format is: 
	// (type of input, stream size, cardinality constraint, window size, dim)
	getline(config_file, header);
	stringstream ss(header);
	ss >> type >> N >> K >> W >> DIM >> offset;
	cerr << "Problem type: " << type << endl;
	cerr << "(n, k, w, dim) = (" << N << ", " << K << ", " << W << ", " << DIM << ")" << endl;
	cerr << "offset = " << offset << endl;

	MetricEvaluator* metric = nullptr;
	BaseSource* source = nullptr;
	if (type == "random") {
		source = new RandomSource(N, DIM);
		metric = new SimilarityMetric;
	}
	else if (type == "embedding") {
		source = new FileSource(filename);
		metric = new SimilarityMetric;
	}
	else if (type == "geolocation") {
		source = new FileSource(filename);
		metric = new GeodistanceMetric;
	}
	else if (type == "binary") {
		source = new BinarySource(filename);
		metric = new SimilarityMetric;
	}
	else {
		assert(false && "Unknown problem type!");
	}

	//////////////////////////////////////////////
	int step = 10000;

	GreedyStreamer gstream(N, K, metric);
	MaxTraceStreamer mstream(N, K, metric);
	ThresholdStreamer tstream(N, K, metric, K * 0.8);

	auto tmi = chrono::steady_clock::now();
	auto tmf = chrono::steady_clock::now();
	auto tgi = chrono::steady_clock::now();
	auto tgf = chrono::steady_clock::now();

/*
	cout << "***Sequential model tests***" << endl;
	tgi = chrono::steady_clock::now();
	for (size_t i = 0; i < N; i++) {
		gstream.add(source->data_at(i));
		if (i % step == 0) {
			cout << gstream.query() << " ";
		}
	}
	cout << endl;

	cout << "Items added successfully." << endl;
	cout << "The greedy objective is: " << gstream.query() << endl;
	tgf = chrono::steady_clock::now();
	cout << "Total time (greedy): " << chrono::duration_cast<chrono::milliseconds>(tgf - tgi).count() << " ms" << endl;
	cout << "Solution: ";
	for (int x : gstream.solution()) {
		cout << x << " ";
	}
	cout << endl;

/*
	// //////////////////////////////////////////////
	tmi = chrono::steady_clock::now();
	for (size_t i = 0; i < N; i++) {
		mstream.add(source->data_at(i));
		if (i % step == 0) {
			cout << mstream.query() << " ";
		}
	}
	cout << endl;

	cout << "Items added successfully." << endl;
	cout << "The max-trace objective is: " << mstream.query() << endl;
	tmf = chrono::steady_clock::now();
	cout << "Total time (max-trace): " << chrono::duration_cast<chrono::milliseconds>(tmf - tmi).count() << " ms" << endl;
	cout << "Solution: ";
	for (int x : mstream.solution()) {
		cout << x << " ";
	}
	cout << endl;

	// //////////////////////////////////////////////

	auto tti = chrono::steady_clock::now();
	for (size_t i = 0; i < N; i++) {
		tstream.add(source->data_at(i));
		if (i % step == 0) {
			cout << tstream.query() << " ";
		}
	}
	cout << endl;
	cout << "Items added successfully." << endl;
	cout << "The threshold objective is: " << tstream.query() << endl;
	auto ttf = chrono::steady_clock::now();
	cout << "Total time (threshold): " << chrono::duration_cast<chrono::milliseconds>(ttf - tti).count() << " ms" << endl;
	cout << "Solution: ";
	for (int x : tstream.solution()) {
		cout << x << " ";
	}
	cout << endl;

	////////////////////////////////////////////// */

	// Variable defined but not used for tests to make sure compiler
	// does not optimize away anything
	//volatile double result __attribute__((unused));

	
	double result = 0;

	cout << endl;
	cout << "***Sliding window model tests***" << endl;
	gstream = GreedyStreamer(W, K, metric);
	mstream = MaxTraceStreamer(W, K, metric);
	WindowedStreamer wstream(W, K, metric, K * 0.8);
	//WindowedStreamer wstream(W, K, metric, K, 0.05);

	size_t FREQ = 1000;
	//size_t FREQ = 100;
	map<size_t, double> outputValue[3];
	map<size_t, vector<size_t>> outputSolution[3];

	
	double total_gval = 0;
	tgi = chrono::steady_clock::now();
	for (size_t i = 0+offset; i < N+offset; i++) {
		gstream.add(source->data_at(i));
	 	result = gstream.query();
	 	if (i > 0 && i % FREQ == 0) { 
			//cout << result << " ";
	 		outputValue[0][i] = result;
	 		outputSolution[0][i] = gstream.solution();
		}
		total_gval += result;
		if (i == 5000) {
			tgf = chrono::steady_clock::now();
			cout << "Total time per ele(greedy): " << chrono::duration_cast<chrono::nanoseconds>(tgf - tgi).count() / (i + 1) << " ms" << endl;
			break;
		}
	}
	cout << "Items added successfully." << endl;
	tgf = chrono::steady_clock::now();
	cout << "Total time (greedy): " << chrono::duration_cast<chrono::milliseconds>(tgf - tgi).count() << " ms" << endl;

	////////////////////////////////////////////// */

	size_t mcs = 0;
	size_t old = 0;
	int count = 0;
	double total_mval = 0;
	tmi = chrono::steady_clock::now();
	for (size_t i = 0+offset; i < N+offset; i++) {
		mstream.add(source->data_at(i));
		result = mstream.query();
		mcs = max(mcs, mstream.cache_size());
		if (i > 0 && i % FREQ == 0) {
			//cout << i << ":" << result << endl;
			outputValue[1][i] = result;
			outputSolution[1][i] = mstream.solution();
		}
		/*
		if (old == mcs) {
			count++;
			if (count == 5) {
				cout << "memory : " << mcs << endl;
				count = 0;
				break;
			}
		}
		old = mcs; */
		if (i == 5000) {
			tmf = chrono::steady_clock::now();
			cout << "Total time per ele (max-trace): " << chrono::duration_cast<chrono::nanoseconds>(tmf - tmi).count() / (i + 1) << " ms" << endl;
			break;
		}
		total_mval += result;
		if (i % step == 0) {
			cout << mstream.query() << " ";
		}
	}
	cout << endl;
	cout << "Items added successfully." << endl;
	tmf = chrono::steady_clock::now();
	cout << "Total time (max-trace): " << chrono::duration_cast<chrono::milliseconds>(tmf - tmi).count() << " ms" << endl;

	////////////////////////////////////////////// */

	size_t wcs = 0;
	double total_wval = 0;
	auto twi = chrono::steady_clock::now();
	for (size_t i = 0+offset; i < N+offset; i++) {
		wstream.add(source->data_at(i));
		result = wstream.query();
		wcs = max(wcs, wstream.cache_size());
		if (i > 0 && i % FREQ == 0) {
			//cout << i << ":" << result << endl;
			outputValue[2][i] = result;
			outputSolution[2][i] = wstream.solution();
		}
		/*
		if (old == wcs) {
			count++;
			if (count == 5) {
				cout << "memory : " << wcs << endl;
				count = 0;
				break;
			}
		}
		old = wcs;*/
		if (i == 5000) {
			auto twf = chrono::steady_clock::now();
			cout << "Total time per ele (windowed): " << chrono::duration_cast<chrono::nanoseconds>(twf - twi).count() / (i + 1) << " ms" << endl;
			break;
		}
		total_wval += result;
		//if (i % step == 0) {
	//		cout << wstream.query() << " ";
	//	}
	}
	cout << endl;
	cout << "Items added successfully." << endl;
	auto twf = chrono::steady_clock::now();
	cout << "Total time (windowed): " << chrono::duration_cast<chrono::milliseconds>(twf - twi).count() << " ms" << endl;

	cout << "Ratio (greedy / windowed): " << total_gval / total_wval << endl;
	cout << "Ratio (max-trace / windowed): " << total_mval / total_wval << endl;
	//cout << "Number of vectors stored in cache (max-trace vs windowed): " << mcs << " " << wcs << endl;

	string alg[] = { "greedy", "max-trace", "windowed" };
	for (int i = 0; i < 3; i++) {
		ofstream value_file("value_" + alg[i] + ".txt");
		ofstream solution_file("solution_" + alg[i] + ".txt");
		for (auto kv : outputValue[i]) {
			value_file << kv.first << " " << kv.second << "\n";
		}
		for (auto kv : outputSolution[i]) {
			solution_file << kv.first << " ";
			for (auto id : kv.second) {
				solution_file << id << " ";
			}
			solution_file << "\n";
		}
		value_file.close();
		solution_file.close();
	}

	delete source;
	delete metric;
	//*/
	return 0;
}

//*/
