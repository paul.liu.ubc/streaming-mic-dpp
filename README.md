# Summary
This repository contains the code for the paper "Diversity on the Go! Streaming Determinantal Point Processes under a Maximum Induced Cardinality Objective", WWW '21.

To use:
```
make
./main -f [input_file]
```

The input files have the following format:
```
[problem_type] [stream_size] [k] [window_size] [dimension]
vector_1
vector_2
...
```
An example problem file is given in `gowalla10.prob`.

The problem type may be one of `embedding`, `geolocation`, `random`, or `binary`. Each corresponds to a different type of DPP kernel. See the paper for more information.
