#pragma once
#ifndef _THRESHOLD_STREAMER_H
#define _THRESHOLD_STREAMER_H

#include "base_streamer.h"

#include <omp.h>

#include <deque>
#include <list>

/* Insertion-only threshold streamer. Subcomponent of various Streaming algs.
 * Not a subclass of base streamer because it does not hold its own cache.
 * Uses a single threshold.
 */
class SingleThresholdStreamer {
	size_t m_k; // The cardinality constraint
	MICObjective m_obj; // The MIC objective function
	shared_ptr<VectorCache> m_cache; // This single threshold algorithm is mostly a
									 // Subroutine of bigger algorithms, so allow it to
									 // Borrow a cache instead of keeping its own.
									 // (This should be the default design actually.)
	double m_thresh; // The threshold value for this instance
public:
	SingleThresholdStreamer(size_t wsize,
		size_t k,
		double thresh,
		shared_ptr<VectorCache> cache)
		: m_k(k), m_obj(cache), m_cache(cache), m_thresh(thresh) {
	}

	// In special cases, the user might need to specify the cache id of the
	// element. This function is only used in the internals of windowed streamer.
	bool add(size_t id) {
		if (m_obj.length() >= m_k) return false;
		bool res = m_obj.threshold_add(id, m_thresh);
		return res;
	}

	double query() {
		return m_obj.value();
	}

	// Number of elements currently in the solution
	size_t length() {
		return m_obj.length();
	}

	// The ids of vectors in the solution
	vector<size_t> solution() {
		return m_obj.ids();
	}
};


/* Insertion-only threshold streamer. Completely ignores the window size.
 * An aggregate of multiple threshold streamers.
 */
class ThresholdStreamer : public BaseStreamer {
	vector<SingleThresholdStreamer> m_subalgs; // The MIC objective function
public:
	ThresholdStreamer(size_t wsize,
		size_t k,
		MetricEvaluator* metric = nullptr,
		double opt_hint = 10,
		double eps = 0.1,
		int thresh_lim = 5) : BaseStreamer(wsize, k, metric) {
		// The opt_hint is a guess within O(k) of the optimal solution. We can 
		// also compute this on the fly in theory.
		for (int i = -thresh_lim; i <= thresh_lim; i++) {
			double thresh = pow(1 + eps, i) * opt_hint / k;
			m_subalgs.push_back(SingleThresholdStreamer(wsize, k, thresh, m_cache));
		}
	}

	// Returns true if element was accepted into the cache,
	// false otherwise.
	bool add(const vector<double>& elt) {
		m_cache->add(m_time, elt);

		bool res = false;
		for (auto& alg : m_subalgs) {
			res = res || alg.add(m_time);
		}
		if (!res) m_cache->remove(m_time);

		m_time++;
		return res;
	}

	double query() {
		double best = 0;
		for (auto& alg : m_subalgs) {
			best = max(best, alg.query());
		}
		return best;
	}

	vector<size_t> solution() {
		double best = 0;
		vector<size_t> best_sol;
		for (auto& alg : m_subalgs) {
			double val = alg.query();
			if (val > best) {
				best = val;
				best_sol = alg.solution();
			}
		}
		return best_sol;
	}
};

/* Windowed threshold streamer. Allows the insertion-only streamer above to
 * handle deletions at the cost of increasing memory to O(\sqrt{nk}).
 */
class WindowedStreamer : public BaseStreamer {
	vector<double> m_thresh; // List of thresholds to check
	double m_eps; // Epsilon value for our parallel guesses
	list<SingleThresholdStreamer> m_subalgs; // The MIC objective function
	size_t m_subwsize; // Subwindow size

	typedef list<SingleThresholdStreamer>::iterator thresh_iterator;
	// Tracker for removing algorithm instances
	map<size_t, vector<thresh_iterator>> m_removals;
	map<size_t, size_t> m_nrefs; // Maintains the total instances of this cache
								 // element over all solutions

public:
	WindowedStreamer(size_t wsize,
		size_t k,
		MetricEvaluator* metric = nullptr,
		double opt_hint = 10,
		double eps = 0.1,
		int thresh_lim = 5) : BaseStreamer(wsize, k, metric) {
		// The opt_hint is a guess within O(k) of the optimal solution. We can 
		// also compute this on the fly in theory.
		for (int i = -thresh_lim; i <= thresh_lim; i++) {
			double thresh = pow(1 + eps, i) * opt_hint / k;
			m_thresh.push_back(thresh);
		}
		m_eps = eps;
		m_subwsize = max(10 * k, (size_t)sqrt(wsize * k));
	}

	// Returns true if element was accepted into the cache,
	// false otherwise.
	bool add(const vector<double>& elt) {
		if (m_time % m_subwsize == 0) {
			// If we're at the beginning of a subwindow, we should initialize
			// a family of new algorithm instances. The memory use of this part
			// can be improved by an O(k) factor with a more complicated 
			// implementation. See paper for more details.
			for (double thresh : m_thresh) {
				auto alg = SingleThresholdStreamer(m_wsize, m_k, thresh, m_cache);
				m_subalgs.push_back(alg);
				for (size_t i = 0; i < m_subwsize; i++) {
					if (i >= m_time) break;

					size_t id = m_time - i - 1;
					bool res = alg.add(id);
					if (res) {
						m_subalgs.push_back(alg);
						m_removals[id].push_back(--m_subalgs.end());

						for (size_t id : alg.solution()) {
							m_nrefs[id]++;
						}
					}
				}
			}
		}

		// See if we need to remove elements from the sliding window
		m_cache->add(m_time, elt);
		if (m_time > m_subwsize) {
			size_t removed_id = m_time - m_subwsize - 1;

			if (m_removals.count(removed_id)) {
				for (auto it : m_removals[removed_id]) {
					// Remove all elements in this algorithm instance
					for (size_t id : it->solution()) {
						m_nrefs[id]--;
						// Assumes contiguous ids
						if (id > removed_id + m_subwsize && m_nrefs[id] == 0) {
							m_cache->remove(id);
							m_nrefs.erase(id);
						}
					}
					// Remove these subalgorithms from the update list
					m_subalgs.erase(it);
				}
				m_removals.erase(removed_id);
			}
			m_cache->remove(removed_id);
		}

		vector<int> t_results;
		vector<thresh_iterator> t_iters;

		for (auto it = m_subalgs.begin(); it != m_subalgs.end(); it++) {
			t_iters.push_back(it);
			t_results.push_back(0);
		}

#pragma omp parallel
		{
#pragma omp for schedule(dynamic)
			for (int i = 0; i < t_iters.size(); i++) {
				t_results[i] = t_iters[i]->add(m_time) * t_iters[i]->length();
			}
		}

		bool any_added = false;
		for (size_t i = 0; i < t_iters.size(); i++) {
			if (t_results[i] == 0) continue;

			m_nrefs[m_time]++;
			if (t_results[i] == 1) {
				m_removals[m_time].push_back(t_iters[i]);
			}
			any_added = true;
		}

		if (m_time % m_subwsize == 0) {
			vector<thresh_iterator> to_delete;
			for (auto it = m_subalgs.begin(); it != m_subalgs.end(); it++) {
				if (it->length() == 0) {
					to_delete.push_back(it);
				}
			}

			// Leave one solution to extend
			if (!to_delete.empty()) {
				to_delete.pop_back();
			}

			for (auto it : to_delete) {
				m_subalgs.erase(it);
			}
		}

		m_time++;
		return any_added;
	}

	double query() {
		double best = 0;
		for (auto& alg : m_subalgs) {
			best = max(best, alg.query());
		}
		return best;
	}

	// The ids of vectors in the solution
	vector<size_t> solution() {
		double best = 0;
		vector<size_t> best_sol;
		for (auto& alg : m_subalgs) {
			double val = alg.query();
			if (val > best) {
				best = val;
				best_sol = alg.solution();
			}
		}
		return best_sol;
	}
};


#endif
